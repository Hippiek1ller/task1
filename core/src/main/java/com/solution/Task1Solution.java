package com.solution;

import com.persons.Gender;
import com.persons.Person;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Task1Solution {

    public static void main(String[] args) {

        Person personToFindInCollection = new Person("Anton", "Chekhov", "Pavlovich", Gender.MALE, LocalDate.of(2001, 1, 26));

        System.out.println("Find person in collection");

        personList.stream().filter(person -> person.equals(personToFindInCollection)).forEach(System.out::println);

        System.out.println();

        System.out.println("Persons younger than 20 years with last name starting with A-D, a-d");

        personList.stream().filter(person -> person.getAge() <= 20).filter(person -> person.getLastName().substring(0, 1).toLowerCase().matches("[a-d]"))
                .forEach(System.out::println);

        System.out.println();

        System.out.println("Persons with duplicates in collection, sorted by age");

        personList
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(p -> p.getValue() == 2)
                .map(Map.Entry::getKey)
                .sorted(Comparator.comparing(Person::getAge).reversed())
                .forEach(System.out::println);

        System.out.println();

        System.out.println("Persons with more than 3 duplicates in collection, sorted by age");

        personList
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(p -> p.getValue() > 3)
                .map(Map.Entry::getKey)
                .sorted(Comparator.comparing(Person::getAge).reversed())
                .forEach(System.out::println);

    }

    private static final List<Person> personList = Arrays.asList(
            new Person("Fyodor", "Tyutchev", "Ivanovich", Gender.MALE, LocalDate.of(1990, 12, 27)),
            new Person("Anton", "Chekhov", "Pavlovich", Gender.MALE, LocalDate.of(2001, 1, 26)),
            new Person("Anton", "Chekhov", "Pavlovich", Gender.MALE, LocalDate.of(2001, 1, 26)),
            new Person("Vladimir", "Nabokov", "Vladimirovich", Gender.MALE, LocalDate.of(1993, 4, 22)),
            new Person("Anna", "Gorenko", "Andreyevna", Gender.FEMALE, LocalDate.of(2002, 6, 23)),
            new Person("Anna", "Gorenko", "Andreyevna", Gender.FEMALE, LocalDate.of(2002, 6, 23)),
            new Person("Fyodor", "Dostoevsky", "Mikhailovich", Gender.MALE, LocalDate.of(2003, 11, 11)),
            new Person("Fyodor", "Dostoevsky", "Mikhailovich", Gender.MALE, LocalDate.of(2003, 11, 11)),
            new Person("Anton", "Chekhov", "Pavlovich", Gender.MALE, LocalDate.of(2001, 1, 26)),
            new Person("Marina", "Tsvetaeva", "Ivanovna", Gender.FEMALE, LocalDate.of(1990, 12, 27)),
            new Person("Anton", "Chekhov", "Pavlovich", Gender.MALE, LocalDate.of(2001, 1, 26)),
            new Person("Ivan", "Turgenev", "Sergeyevich", Gender.MALE, LocalDate.of(1970, 9, 3)),
            new Person("Ivan", "Turgenev", "Sergeyevich", Gender.MALE, LocalDate.of(1970, 9, 3)),
            new Person("Ivan", "Bunin", "Alekseyevich", Gender.MALE, LocalDate.of(2003, 11, 8)),
            new Person("Fyodor", "Dostoevsky", "Mikhailovich", Gender.MALE, LocalDate.of(2003, 11, 11)),
            new Person("Fyodor", "Dostoevsky", "Mikhailovich", Gender.MALE, LocalDate.of(2003, 11, 11)),
            new Person("Fyodor", "Dostoevsky", "Mikhailovich", Gender.MALE, LocalDate.of(2003, 11, 11)));
}
